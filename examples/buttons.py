import minitk

def button1():
    print('button 1')

def button2():
    print('button 2')

def button3():
    print('button 3')

def button4():
    print('button 4')


control_panel = minitk.ControlPanel()
control_panel.create_button('Button 1', button1, column=0)
control_panel.create_button('Button 2', button2, column=1)
control_panel.create_button('Button 3', button3, column=0)
control_panel.create_button('Button 4', button4, column=1)

while True:
    control_panel.update()