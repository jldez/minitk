import minitk

def say_hello():
    print('hello')

def set_status(status:bool):
    print(f"Status set to: {status}")

def set_value(value:float):
    print(f"Value set to: {value}")

def set_option(option:str):
    print(f"Option set to: {option}")


control_panel = minitk.ControlPanel()
control_panel.create_button('Say hello', say_hello)
control_panel.create_check_button('Status', callback=set_status)
control_panel.create_slider('Value', 0, 10, callback=set_value)
control_panel.create_menu('Options', ['a', 'b', 'c'], callback=set_option)

while True:
    control_panel.update()