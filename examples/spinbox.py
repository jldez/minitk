import minitk

def on_change(option: int):
    print(f"You selected {option}")


control_panel = minitk.ControlPanel()
control_panel.create_spinbox(name="Spinbox", callback=on_change)

while True:
    control_panel.update()
