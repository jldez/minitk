import minitk



class App:

    def __init__(self):

        self.control_panel = minitk.ControlPanel('App', exit_callback=self.close)

        self.control_panel.create_button('Bonjour', self.bonsoir)
        self.control_panel.create_check_button('Status', callback=self.set_status)
        self.control_panel.create_slider('Value', 0, 10, callback=self.set_value)
        self.control_panel.create_scrollmenu('Options', ['a', 'b', 'c'], callback=self.set_option, editable=True)

        self.control_panel.add_separator()

        self.control_panel.start_container('Additional Parameters')
        self.control_panel.create_button('Sum', self.sum_parameters)
        self.control_panel.create_slider('P1', 0, 10)
        self.control_panel.create_slider('P2', 0, 10)
        self.control_panel.create_slider('P3', 0, 10)
        self.control_panel.end_container('Additional Parameters')

        self.running = False

    
    def run(self):
        self.running = True
        while self.running:
            self.update()


    def update(self):
        self.control_panel.update()


    def bonsoir(self):
        print('Bonsoir')

    def set_status(self, status:bool):
        print(f"Status set to: {status}")

    def set_value(self, value:float):
        print(f"Value set to: {value}")

    def set_option(self, option:str):
        print(f"Option set to: {option}")

    def sum_parameters(self):
        print(f"Sum of parameters is {sum([self.control_panel.get_state(name) for name in ['P1','P2','P3']])}")


    def close(self):
        self.running = False



if __name__ == "__main__":
    app = App()
    app.run()