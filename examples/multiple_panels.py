import minitk


control_panel = minitk.ControlPanel('Actions')
control_panel.create_button('Action 1')
control_panel.create_button('Action 2')
control_panel.create_button('Action 3')
control_panel.create_button('Action 4')
control_panel.create_button('Action 5')

control_panel_2 = minitk.ControlPanel('Other actions')
control_panel_2.create_button('Action 6')
control_panel_2.create_button('Action 7')
control_panel_2.create_button('Action 8')
control_panel_2.create_button('Action 9')
control_panel_2.create_button('Action 10')

control_panel.attach(control_panel_2)


while True:
    control_panel.update()