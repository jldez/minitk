import minitk


control_panel = minitk.ControlPanel()
control_panel.start_container('Parameters')
control_panel.create_slider('a', 0, 10)
control_panel.create_slider('b', 0, 10)
control_panel.create_slider('c', 0, 10)
control_panel.create_slider('d', 0, 10)
control_panel.create_slider('e', 0, 10)
control_panel.end_container('Parameters')

while True:
    control_panel.update()