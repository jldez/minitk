import minitk

def on_hotkey1_press():
    print('hotkey 1')

def on_hotkey2_press():
    print('hotkey 2')

def on_hotkey3_press():
    print('hotkey 3')

def on_hotkey4_press(value):
    print(f'hotkey 4 ({value})')


control_panel = minitk.ControlPanel()
control_panel.create_button('Action 1', on_hotkey1_press, hotkey='1')
control_panel.create_button('Action 2', on_hotkey2_press, hotkey='2')
control_panel.create_button('Action 3', on_hotkey3_press, hotkey='3')
control_panel.create_check_button('Check', on_hotkey4_press, hotkey='4')
control_panel.create_button('Quit', exit, hotkey='q')

while True:
    control_panel.update()