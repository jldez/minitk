import minitk

def on_change(option: str):
    print(f"You selected {option}")


control_panel = minitk.ControlPanel()
control_panel.create_radiomenu(name="Choose", choices=["choice a", "choice b", "choice c"], default="choice a", callback=on_change)

while True:
    control_panel.update()
